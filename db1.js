var http = require('http');
var mysql = require('mysql');
var bodyparser = require('body-parser');
var express = require('express');
var sequelize = require('sequelize');
var connection = require('./dbConnection');

var app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.set('port', process.env.PORT || 5000);

app.get('/users', function(req, res) {
  connection.query('select * from Users', function(err, rows, fields) {
    if (!err) {
      res.json({ data: rows });
    } else {
      res.status(400).send(err);
    }
  });
});

app.get('/users/:id', function(req, res) {
  var id = req.params.id;

  connection.query('SELECT * from Users where id = ?', [id], function(
    err,
    rows,
    fields
  ) {
    if (!err) {
      if (rows.length != 0) {
        res.json({ data: rows });
      }
    } else {
      res.status(400).send(err);
    }
  });
});

app.post('/user', function(req, res) {
  var post = {
    // id: req.params.id,
    username: req.body.username,
    createdAt: new Date(),
    updatedAt: new Date()
  };
  connection.query('Insert into Users set ?', post, function(err, result) {
    if (!err) {
      if (result.affectedRows != 0) {
        res.json({ result: 'success' });
      }
    } else {
      res.status(400).send(err);
    }
  });
});

app.put('/user/:id', function(req, res) {
  var id = req.params.id;
  var username = req.body.username;
  var createdAt = new Date();
  var updatedAt = new Date();

  connection.query(
    'update Users set username = ?,createdAt = ? ,updatedAt = ?  where id = ?',
    [username, createdAt, updatedAt, id],
    function(err, result) {
      if (!err) {
        if (result.affectedRows != 0) {
          res.json({ result: 'success' });
        } else {
          res.status(400).send(err);
        }
      }
    }
  );
});

app.delete('/user/:id', function(req, res) {
  var id = req.params.id;
  connection.query('delete from Users where id = ?', [id], function(
    err,
    rows,
    fields
  ) {
    if (!err) {
      res.json({ result: 'success' });
    } else {
      res.status(400).send(err);
    }
  });
});
