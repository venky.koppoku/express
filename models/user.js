'use strict';

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    username: { type: DataTypes.STRING, notEmpty: true }
  });

  User.associate = function(models) {
    User.hasMany(models.Task);
  };

  return User;
};