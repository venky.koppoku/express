import test from 'ava';
const request = require('supertest');
const app = require('../bin/www');

test('get all users', async t => {
  t.plan(1);
  const res = await request(app).get('/users');
  t.is(res.status, 200);
});

test('get user data of id 1', async t => {
  t.plan(2);
  const res = await request(app).get('/users/1');
  t.is(res.status, 200);
  t.is(res.body.username, 'venky');
});

test('save user data', async t => {
  t.plan(1);
  const res = await request(app)
    .post('/users')
    .send({ username: 'raj' });
  t.is(res.status, 200);
});

test('save user data without privide username', async t => {
  t.plan(1);
  const res = await request(app).post('/users');
  t.is(res.status, 500);
});

test('update the user name of id 3', async t => {
  t.plan(1);
  const res = await request(app)
    .put('/users/3')
    .send({ username: 'rich' });
  t.is(res.status, 200);
});

test('delete the user data of id 28', async t => {
  t.plan(1);
  const res = await request(app).delete('/users/28');
  t.is(res.status, 200);
});

test('delete the user data ', async t => {
  t.plan(2);
  const res = await request(app).delete('/users/28');
  t.is(res.status, 200);
  t.is(res.body.result, 'no record deleted');
});
