var express = require('express');
var router = express.Router();
var User = require('../models').User;
var mysql = require('mysql');
var Sequelize = require('sequelize');

router.get('/', async (req, res, next) => {
  const usrs = await User.findAll();
  res.json(usrs);
});

/* GET users listing. */
router.get('/:id', async (req, res, next) => {
  const usrs = await User.findById(req.params.id);
  res.json(usrs);
});

router.post('/', async (req, res, next) => {
  try {
    const usr = await User.create({
      username: req.body.username
    });
  } catch (e) {
    return next(e);
  }

  return res.status(200).json({ result: 'success' });
});

router.put('/:id', async (req, res, next) => {
  const id1 = req.params.id;
  try {
    const update = await User.update(
      {
        username: req.body.username
      },
      {
        where: {
          id: {
            $eq: id1
          }
        }
      }
    );
    console.log(update);
  } catch (e) {
    return next(e);
  }
  res.json({ result: 'success' });
});

router.delete('/:id', async (req, res, next) => {
  const id1 = req.params.id;
  const dlt = await User.destroy({
    where: {
      id: id1
    }
  });
  if (dlt !== 0) res.json({ result: 'success' });
  else res.json({ result: 'no record deleted' });
});

module.exports = router;
