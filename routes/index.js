const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const auth = require('../auth.js')();
const users = require('../users.js');
const jwt = require('jwt-simple');
const cfg = require('../config.js');

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get('/', function(req, res) {
  res.json({
    status: 'My api alive'
  });
});

router.get('/user', auth.authenticate(), function(req, res) {
  res.json(users[req.user.id]);
});

router.post('/token', function(req, res) {
  if (req.body.email && req.body.password) {
    const email = req.body.email;
    const password = req.body.password;
    const user = users.find(function(u) {
      return u.email === email && u.password === password;
    });
    if (user) {
      const payload = {
        id: user.id
      };
      const token = jwt.encode(payload, cfg.jwtSecret);
      res.json({
        token: token
      });
    } else {
      res.sendStatus(401);
    }
  } else {
    res.sendStatus(401);
  }
});

module.exports = router;
